# Battleships

Simple battleship game for one player without computer enemy.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

- [.NET Core 2.2 SDK](https://dotnet.microsoft.com/download)
- [Visual Studio 2019](https://visualstudio.microsoft.com/)

### Installing & Testing

Clone or download and unpack repository.

To run and test application using visual studio:

1. Open Battleships.sln file
2. Make sure that starting project is set to `Battleships`

To run application using command line:

1. Open command line tool
2. Go to directory where project was downloaded and navigate to `\Battleships\Battleships` path
3. Execute command

```
dotnet build
dotnet run
```

To test application using command line:

1. Open command line tool
2. Go to directory where project was downloaded and navigate to `\Battleships\Battleships.Tests` path
3. Execute command

```
dotnet build
dotnet test
```

## Built With

- [.NET Core 2.2](https://dotnet.microsoft.com/download)
- [FluentAssertions](https://fluentassertions.com/)
- [Moq](https://github.com/moq/moq4)
- [NUnit](https://nunit.org/)
