﻿using Battleships.Game;
using Battleships.Interfaces;
using Battleships.Models;
using Moq;
using System.Collections.Generic;
using System.Linq;

namespace Battleships.UnitTests.GameTests
{
    public class GameTestsBase
    {
        protected Mock<IGameSetup> MockGameSetup = new Mock<IGameSetup>();
        protected Mock<IPlayerInputParser> MockPlayerInputParser = new Mock<IPlayerInputParser>();

        protected Board CreateEmptyBoard()
        {
            var gameSetup = new GameSetup();
            return gameSetup.CreateBoard();
        }

        protected Board CreateBoardWithPlacedShips(List<Ship> ships)
        {
            var gameSetup = new GameSetup();
            var board = gameSetup.CreateBoard();

            var shipsPositions = ships.SelectMany(s => s.Position);

            foreach (var tile in board.Tiles)
            {
                if (shipsPositions.Contains(tile.Coordinates))
                {
                    tile.IsOccupied = true;
                }
            }

            return board;
        }
    }
}
