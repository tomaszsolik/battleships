﻿using Battleships.Models;
using FluentAssertions;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;

namespace Battleships.UnitTests.GameTests
{
    [TestFixture]
    public class GameTests : GameTestsBase
    {
        [Test]
        public void StartGame_EmptyBoard_GameFinishedSuccessfully()
        {
            // Arrange
            var existingShips = new List<Ship>
            {
                new Battleship
                {
                    Position = new List<Point>
                    {
                        new Point { X = 6, Y = 4 },
                        new Point { X = 6, Y = 5 },
                        new Point { X = 6, Y = 6 },
                        new Point { X = 6, Y = 7 },
                        new Point { X = 6, Y = 8 }
                    }
                },
                new Destroyer
                {
                    Position = new List<Point>
                     {
                        new Point { X = 1, Y = 4 },
                        new Point { X = 1, Y = 5 },
                        new Point { X = 1, Y = 6 },
                        new Point { X = 1, Y = 7 },
                    }
                },
                new Destroyer
                {
                    Position = new List<Point>
                     {
                        new Point { X = 2, Y = 2 },
                        new Point { X = 3, Y = 2 },
                        new Point { X = 4, Y = 2 },
                        new Point { X = 5, Y = 2 },
                    }
                }
            };

            var existingBoard = CreateBoardWithPlacedShips(existingShips);

            MockGameSetup.Setup(m => m.CreateBoard()).Returns(existingBoard);
            MockGameSetup.Setup(m => m.CreateShips()).Returns(existingShips);
            MockGameSetup.Setup(m => m.PlaceShips(It.IsAny<Board>(), It.IsAny<List<Ship>>()))
                .Callback<Board, List<Ship>>((b, _) => b = existingBoard);

            var positions = existingShips.SelectMany(s => s.Position).ToList();

            var positionsIndex = 0;

            MockPlayerInputParser
                .Setup(m => m.TryParsePlayerInput(out It.Ref<int>.IsAny, out It.Ref<int>.IsAny))
                .Returns(new TryParsePlayerInputCallback((out int column, out int row) =>
                {
                    column = positions[positionsIndex].X;
                    row = positions[positionsIndex].Y;
                    positionsIndex++;
                    return true;
                }));

            MockPlayerInputParser.Setup(m => m.RestartGame()).Returns(false);

            var game = new Game.Game(MockGameSetup.Object, MockPlayerInputParser.Object);

            // Act
            game.StartGame();

            // Assert
            existingBoard.Tiles.Where(t => t.IsOccupied == false).Should().HaveCount(100, "all ships were destroyed");

            MockGameSetup.Verify(m => m.CreateBoard(), Times.Once);
            MockGameSetup.Verify(m => m.CreateShips(), Times.Once);
            MockGameSetup.Verify(m => m.PlaceShips(It.IsAny<Board>(), It.IsAny<List<Ship>>()), Times.Once);

            MockPlayerInputParser.Verify(m => m.RestartGame(), Times.Once);
        }

        delegate bool TryParsePlayerInputCallback(out int column, out int row);
    }
}
