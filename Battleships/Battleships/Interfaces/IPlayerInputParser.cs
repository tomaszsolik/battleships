﻿namespace Battleships.Interfaces
{
    /// <summary>
    /// The class responsible for handling player input.
    /// </summary>
    public interface IPlayerInputParser
    {
        /// <summary>
        /// Parsing player input.
        /// </summary>
        /// <param name="column">The column value parsed from player input.</param>
        /// <param name="row">he column value parsed from player input.</param>
        /// <returns><b>True</b> if input values were correct. Otherwise <b>false</b>.</returns>
        bool TryParsePlayerInput(out int column, out int row);

        /// <summary>
        /// Check if game should be restarted.
        /// </summary>
        /// <returns><b>True</b> if player pressed 'Y' key. Otherwise <b>false</b>.</returns>
        bool RestartGame();
    }
}