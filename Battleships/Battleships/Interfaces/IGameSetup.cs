﻿using Battleships.Models;
using System.Collections.Generic;

namespace Battleships.Interfaces
{
    /// <summary>
    /// Interface for game setup.
    /// </summary>
    public interface IGameSetup
    {
        /// <summary>
        /// Creates ships for new game.
        /// </summary>
        /// <returns>The list of <see cref="Ship"/> objects.</returns>
        List<Ship> CreateShips();

        /// <summary>
        /// Creates board for new game.
        /// </summary>
        /// <returns>The <see cref="Board"/> object.</returns>
        Board CreateBoard();

        /// <summary>
        /// Places ships on game board.
        /// </summary>
        /// <param name="board">The board where ships will be placed.</param>
        /// <param name="ships">The ships to be placed.</param>
        void PlaceShips(Board board, List<Ship> ships);
    }
}
