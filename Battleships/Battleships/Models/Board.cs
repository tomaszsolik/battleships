﻿using System.Collections.Generic;

namespace Battleships.Models
{
    /// <summary>
    /// The model for game board.
    /// </summary>
    public class Board
    {
        /// <summary>
        /// The tiles of the game board.
        /// </summary>
        public List<Tile> Tiles { get; set; }
    }
}
