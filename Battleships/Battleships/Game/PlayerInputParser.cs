﻿using Battleships.Interfaces;
using System;

namespace Battleships.Game
{
    public class PlayerInputParser : IPlayerInputParser
    {
        public bool TryParsePlayerInput(out int column, out int row)
        {
            row = 0;
            column = 0;

            Console.WriteLine("\nShoot Tile:");
            var input = Console.ReadLine();

            if (input.Length <= 1)
            {
                Console.WriteLine("Please provide proper value");
                return false;
            }

            column = char.ToUpper(input[0]) - 64;
            if (!int.TryParse(input.Substring(1), out row) || !char.IsLetter(input[0]))
            {
                Console.WriteLine("Please provide value in a proper format. For example 'A5'.");
                return false;
            }
            else if (IsNotInRange(row) || IsNotInRange(column))
            {
                Console.WriteLine("Please provide value in range between 1 and 10 or A to J.");
                return false;
            }

            return true;
        }

        public bool RestartGame()
        {

            if (Console.ReadKey().Key == ConsoleKey.Y)
            {
                Console.Clear();
                return true;
            }
            else if (Console.ReadKey().Key == ConsoleKey.N)
            {
                Console.WriteLine("Thanks for playing!");
                return false;
            }
            else
            {
                Console.WriteLine("Please use 'Y' or 'N' key.");
                return RestartGame();
            }
        }

        private bool IsNotInRange(int value)
        {
            return value <= 0 || value > 10;
        }
    }
}
