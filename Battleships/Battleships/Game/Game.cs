﻿using Battleships.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using Battleships.Interfaces;

namespace Battleships.Game
{
    /// <summary>
    /// Class responsible for starting new game.
    /// </summary>
    public class Game
    {
        private readonly IGameSetup _gameSetup = null;
        private readonly IPlayerInputParser _playerInputParser = null;

        /// <summary>
        /// Creates new game object.
        /// </summary>
        /// <param name="gameSetup">The game setup service</param>
        /// <param name="playerInputParser"></param>
        public Game(IGameSetup gameSetup, IPlayerInputParser playerInputParser)
        {
            _gameSetup = gameSetup;
            _playerInputParser = playerInputParser;
        }

        /// <summary>
        /// Starts new game.
        /// </summary>
        public void StartGame()
        {
            var ships = _gameSetup.CreateShips();
            var board = _gameSetup.CreateBoard();

             _gameSetup.PlaceShips(board, ships);

            PlayGame(board, ships, ships.Count());

            Console.WriteLine("Game finished. Do You want to play again?[y/n]");

            if (_playerInputParser.RestartGame())
            {
                ships = _gameSetup.CreateShips();
                board = _gameSetup.CreateBoard();

                _gameSetup.PlaceShips(board, ships);

                PlayGame(board, ships, ships.Count());
            }
            else
            {
                Console.WriteLine("Thanks for playing!");
            }
        }

        private bool PlayGame(Board board, List<Ship> ships, int shipsToDestroy)
        {
            if (_playerInputParser.TryParsePlayerInput(out int column, out int row))
            {
                var shotTile = board.Tiles.First(t => t.Coordinates.X == column && t.Coordinates.Y == row);

                if (shotTile.IsOccupied)
                {
                    Console.WriteLine("That's a direct hit!");
                    shotTile.IsOccupied = false;

                    var shotShip = ships.Find(ship => ship.Position.Contains(new Point { X = column, Y = row }));
                    shotShip.Hits += 1;

                    if (shotShip.Size == shotShip.Hits)
                    {
                        Console.WriteLine($"{shotShip.GetType().Name} was destroyed!");
                        shipsToDestroy--;
                    }

                    if (shipsToDestroy == 0)
                    {
                        Console.WriteLine("Congratulations! You've won!");
                        return true;
                    }
                }
                else
                {
                    Console.WriteLine("That's a miss.");
                }
            }

            return PlayGame(board, ships, shipsToDestroy);
        }
    }
}
