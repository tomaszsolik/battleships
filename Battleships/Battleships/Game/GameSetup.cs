﻿using Battleships.Interfaces;
using Battleships.Models;
using System.Collections.Generic;
using System.Drawing;

namespace Battleships.Game
{
    public class GameSetup : IGameSetup
    {
        public Board CreateBoard()
        {
            var board = new Board
            {
                Tiles = new List<Tile>()
            };

            for (int i = 1; i <= 10; i++)
            {
                for (int j = 1; j <= 10; j++)
                {
                    board.Tiles.Add(new Tile
                    {
                        Coordinates = new Point
                        {
                            X = i,
                            Y = j
                        }
                    });
                }
            }

            return board;
        }

        public List<Ship> CreateShips()
        {
            return new List<Ship>
            {
                new Battleship(),
                new Destroyer(),
                new Destroyer()
            };
        }

        public void PlaceShips(Board board, List<Ship> ships)
        {
            ships.ForEach(ship => ShipPlacer.PlaceShip(board, ship));
        }
    }
}
